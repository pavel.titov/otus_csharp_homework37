﻿namespace Homework37.Settings
{
    public interface IGameSettings
    {
        int From { get; }
        int To { get; }
        int TryCount { get; }
    }
}