﻿namespace Homework37.Settings
{
    public class GameSettings : IGameSettings
    {
        public int TryCount { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }
}
