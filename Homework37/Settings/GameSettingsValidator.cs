﻿using System;

namespace Homework37.Settings
{
    public static class GameSettingsValidator
    {
        public static void Validate(GameSettings gameSettings)
        {
            if (gameSettings.TryCount < 1)
            {
                throw new ArgumentException("Try count must be > 0");
            }
            if (gameSettings.From < 1)
            {
                throw new ArgumentException("From must be > 0");
            }
            if (gameSettings.To < gameSettings.From)
            {
                throw new ArgumentException("To must be > From");
            }
        }
    }
}