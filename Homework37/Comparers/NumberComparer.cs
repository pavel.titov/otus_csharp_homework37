﻿using System;

namespace Homework37.Comparers
{
    class NumberComparer : INumberComparer
    {
        public ComparisonResult Compare(int a, int b)
        {
            return a.CompareTo(b) switch
            {
                -1 => ComparisonResult.Lower,
                1 => ComparisonResult.Higher,
                0 => ComparisonResult.Equal,
                _ => throw new InvalidOperationException()
            };
        }
    }
}
