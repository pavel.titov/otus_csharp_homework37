﻿namespace Homework37.Comparers
{
    interface INumberComparer
    {
        ComparisonResult Compare(int a, int b);
    }
}