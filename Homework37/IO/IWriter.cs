﻿namespace Homework37.IO
{
    interface IWriter
    {
        void Write(string text);
        void WriteDanger(string text);
        void WriteSuccess(string text);
        void WriteWarning(string text);
    }
}
