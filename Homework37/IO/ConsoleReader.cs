﻿using System;

namespace Homework37.IO
{
    public class ConsoleReader : IReader
    {
        public int Read()
        {
            return int.Parse(Console.ReadLine());
        }
    }
}
