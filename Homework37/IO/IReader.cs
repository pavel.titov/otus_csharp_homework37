﻿namespace Homework37.IO
{
    interface IReader
    {
        int Read();
    }
}
