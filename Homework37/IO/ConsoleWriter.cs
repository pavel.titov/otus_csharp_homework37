﻿using System;

namespace Homework37.IO
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string text)
        {
            Console.WriteLine(text);
        }

        public virtual void WriteDanger(string text)
        {
            Write(text);
        }

        public virtual void WriteSuccess(string text)
        {
            Write(text);
        }

        public virtual void WriteWarning(string text)
        {
            Write(text);
        }
    }
}