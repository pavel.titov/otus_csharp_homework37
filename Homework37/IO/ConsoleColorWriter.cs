﻿using System;

namespace Homework37.IO
{
    class ConsoleColorWriter : ConsoleWriter
    {
        private void WriteColor(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Write(text);
            Console.ResetColor();
        }

        public override void WriteDanger(string text)
        {
            WriteColor(text, ConsoleColor.Red);
        }

        public override void WriteSuccess(string text)
        {
            WriteColor(text, ConsoleColor.Green);
        }

        public override void WriteWarning(string text)
        {
            WriteColor(text, ConsoleColor.Yellow);
        }
    }
}
