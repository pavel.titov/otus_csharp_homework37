﻿using Homework37.Comparers;
using Homework37.IO;
using System;

namespace Homework37
{
    class NumberGameController
    {
        private readonly IReader _reader;
        private readonly IWriter _writer;
        private readonly NumberGame _game;

        public NumberGameController(IReader reader, IWriter writer, NumberGame game)
        {
            _reader = reader;
            _writer = writer;
            _game = game;
        }

        public void Play()
        {
            _game.Setup();
            WriteWelcome();
            Loop();
            WriteGameResult();
        }

        private void WriteWelcome()
        {
            _writer.Write($"Guess the conceived number from {_game.Settings.From} to {_game.Settings.To}");
            _writer.Write($"You have {_game.Settings.TryCount} tries\n");
        }

        public void Loop()
        {
            while (_game.Status == GameStatus.InProcess)
            {
                int number = ReadGuess();

                var comparisonResult = _game.MakeGuess(number);

                WriteComparisonResult(comparisonResult);
            }
        }
        private int ReadGuess()
        {
            _writer.Write($"Try {_game.NextTry}. Enter a number:");
            return _reader.Read();
        }

        private void WriteComparisonResult(ComparisonResult? result)
        {
            switch (result)
            {
                case ComparisonResult.Lower:
                    _writer.WriteWarning("Number is less than conceived");
                    break;
                case ComparisonResult.Higher:
                    _writer.WriteWarning("Number is higher than conceived");
                    break;
                default:
                    break;
            }

            _writer.Write("");
        }

        private void WriteGameResult()
        {
            switch (_game.Status)
            {
                case GameStatus.Win:
                    _writer.WriteSuccess("You won!");
                    break;
                case GameStatus.Loss:
                    _writer.WriteDanger($"You lost. Conceived number: {_game.ConceivedNumber}");
                    break;
                case GameStatus.InProcess:
                    throw new ArgumentException(nameof(_game.Status));
            }
        }
    }
}
