﻿using Homework37.Comparers;
using Homework37.Generators;
using Homework37.IO;
using Homework37.Settings;
using Microsoft.Extensions.Configuration;

namespace Homework37
{
    class Program
    {
        static void Main(string[] args)
        {
            var cb = new ConfigurationBuilder().AddJsonFile("appsettings.json", false).Build();
            var settings = cb.GetSection("GameSettings").Get<GameSettings>();
            GameSettingsValidator.Validate(settings);

            var numberGenerator = new NumberGenerator();
            var numberComparer = new NumberComparer();
            var consoleReader = new ConsoleReader();
            var consoleWriter = new ConsoleColorWriter();

            var game = new NumberGame(numberGenerator, numberComparer, settings);
            var gameController = new NumberGameController(consoleReader, consoleWriter, game);

            gameController.Play();
        }
    }
}
