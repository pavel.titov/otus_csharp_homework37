﻿using Homework37.Settings;
using Homework37.Comparers;
using Homework37.Generators;

namespace Homework37
{
    class NumberGame
    {
        private readonly INumberGenerator _numberGenerator;
        private readonly INumberComparer _comparer;

        public IGameSettings Settings { get; private set; }

        private int _triesMade;

        public GameStatus Status { get; private set; }
        public int ConceivedNumber { get; private set; }
        public int NextTry { get => _triesMade + 1; }

        public NumberGame(INumberGenerator generator, INumberComparer comparer, IGameSettings settings)
        {
            _numberGenerator = generator;
            _comparer = comparer;
            Settings = settings;
        }

        public void Setup()
        {
            Status = GameStatus.InProcess;
            ConceivedNumber = _numberGenerator.Generate(Settings.From, Settings.To);
            _triesMade = 0;
        }

        private ComparisonResult CompareNumbers(int number)
        {
            return _comparer.Compare(number, ConceivedNumber);
        }

        public ComparisonResult? MakeGuess(int number)
        {
            ++_triesMade;

            var comparisonResult = CompareNumbers(number);

            if (comparisonResult == ComparisonResult.Equal)
            {
                Status = GameStatus.Win;
            }
            else if (_triesMade == Settings.TryCount)
            {
                Status = GameStatus.Loss;
            }

            return comparisonResult;
        }
    }
}
