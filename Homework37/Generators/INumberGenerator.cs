﻿namespace Homework37.Generators
{
    interface INumberGenerator
    {
        int Generate(int from, int to);
    }
}
