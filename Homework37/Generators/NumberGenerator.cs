﻿using System;

namespace Homework37.Generators
{
    public class NumberGenerator : INumberGenerator
    {
        private readonly Random _random = new();

        public int Generate(int from, int to)
        {
            return _random.Next(from, to);
        }
    }
}
