﻿namespace Homework37
{
    public class Configuration
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int Attempts { get; set; }
    }

}
